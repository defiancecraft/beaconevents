package com.archeinteractive.beaconevents;

import com.archeinteractive.beaconevents.config.components.Beacon;
import com.archeinteractive.beaconevents.config.components.Item;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import java.util.Random;

public class BeaconEventTask extends BukkitRunnable {

    private Beacon beacon = null;
    private int iteration = 0;
    private Random random = new Random(System.currentTimeMillis());

    public BeaconEventTask(Beacon beacon) {
        this.beacon = beacon;
    }

    @Override
    public void run() {
        if (iteration >= getTargetIterations()) {
            this.cancel();
            return;
        }

        Item item = beacon.getItems().get(random.nextInt(beacon.getItems().size()));
        ItemStack itemStack = item.createItemStack();
        iteration += 1;

        if (itemStack == null) {
            return;
        }

        Bukkit.getWorld(beacon.getLocation().getWorld()).dropItem(new Location(Bukkit.getWorld(beacon.getLocation().getWorld()),
                beacon.getLocation().getX() + .5,
                beacon.getLocation().getY() + 1.2,
                beacon.getLocation().getZ() + .5),
                itemStack).setVelocity(new Vector(getRandomDouble(), 0.5, getRandomDouble()));

    }

    public int getTargetIterations() {
        return BeaconEvents.getInstance().getSettings().getEventDuration() / beacon.getFrequency();
    }

    public double getRandomDouble() {
        return random.nextBoolean() ? random.nextDouble() * .15 : random.nextDouble() * -.15;
    }

}
