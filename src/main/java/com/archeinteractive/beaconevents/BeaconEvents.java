package com.archeinteractive.beaconevents;

import com.archeinteractive.beaconevents.config.Settings;
import com.archeinteractive.beaconevents.editor.EditManager;
import com.archeinteractive.beaconevents.utils.JsonConfig;
import com.archeinteractive.beaconevents.utils.command.CommandController;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;

public class BeaconEvents extends JavaPlugin {

    private static BeaconEvents instance;
    private static File settingsFile;
    private Settings settings;
    private BeaconManager beaconManager;

    public void onEnable() {
        instance = this;
        settingsFile = new File(getDataFolder(), "settings.json");

        loadSettings();
        Bukkit.getPluginManager().registerEvents(new EditManager(), this);
        CommandController.registerCommands(this, new BeaconCommands());
        beaconManager = new BeaconManager();
    }

    public void onDisable() {
        settings.save(new File(getDataFolder(), "settings.json"));
    }

    public static BeaconEvents getInstance() {
        return instance;
    }

    public static File getSettingsFile() {
        return settingsFile;
    }

    public Settings getSettings() {
        return settings;
    }

    protected void loadSettings() {
        if (!getDataFolder().exists()) {
            getLogger().info("Config folder not found! Creating...");
            getDataFolder().mkdir();
        }

        settings = JsonConfig.load(settingsFile, Settings.class);
    }

}
