package com.archeinteractive.beaconevents;

import com.archeinteractive.beaconevents.editor.EditManager;
import com.archeinteractive.beaconevents.utils.command.SubCommandHandler;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import java.io.File;

public class BeaconCommands {

    @SubCommandHandler(parent = "beacon", name = "add", permission = "beaconevents.add")
    public void add(Player sender, String[] args) {
        if (EditManager.getInstance().isActive(sender.getName())) {
            sender.sendMessage(ChatColor.RED + "You are already adding a beacon. Please left click a block!");
        } else {
            EditManager.getInstance().setActive(sender.getName());
            sender.sendMessage(ChatColor.GREEN + "Left click a block to add it as a beacon!");
        }
    }

    @SubCommandHandler(parent = "beacon", name = "quit", permission = "beaconevents.quit")
    public void quit(Player sender, String[] args) {
        if (EditManager.getInstance().isActive(sender.getName())) {
            EditManager.getInstance().setInactive(sender.getName());
            sender.sendMessage(ChatColor.GREEN + "You are no longer editing a beacon.");
        } else {
            sender.sendMessage(ChatColor.RED + "You are not editing a beacon at the moment.");
        }
    }

    @SubCommandHandler(parent = "beacon", name = "config", permission = "beaconevents.config")
    public void config(Player sender, String[] args) {
        if (args.length != 1) {
            sender.sendMessage(ChatColor.GOLD + "USAGE: " + ChatColor.GREEN + "/beacon config <save|reload>");
            return;
        }

        switch (args[0].toLowerCase()) {
            case "save":
                BeaconEvents.getInstance().getSettings().save(BeaconEvents.getSettingsFile());
                sender.sendMessage(ChatColor.GREEN + "BeaconEvents config has been saved!");
                break;
            case "reload":
                BeaconEvents.getInstance().loadSettings();
                sender.sendMessage(ChatColor.GREEN + "BeaconEvents config has been reloaded!");
                break;
            default:
                sender.sendMessage(ChatColor.GOLD + "USAGE: " + ChatColor.GREEN + "/beacon config <save|reload>");
                break;
        }
    }

}
