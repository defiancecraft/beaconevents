package com.archeinteractive.beaconevents;

import com.archeinteractive.beaconevents.config.components.Message;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

public class BeaconAnnounceTask extends BukkitRunnable {

    private BeaconManager manager;
    private long end;
    private long nextEventMessage;

    public BeaconAnnounceTask(BeaconManager manager, long end) {
        this.manager = manager;
        this.end = end;
        this.nextEventMessage = System.currentTimeMillis() + BeaconEvents.getInstance().getSettings().getBroadcasts().getEvent().getFrequency() * 1000;
    }

    @Override
    public void run() {
        if (System.currentTimeMillis() > end) {
            for (Player player : Bukkit.getOnlinePlayers()) {
                if (manager.beaconInWorld(player.getWorld().getName())) {
                    for(String message : BeaconEvents.getInstance().getSettings().getBroadcasts().getPostEvent()) {
                        player.sendMessage(manager.formatString(message));
                    }
                }
            }
            this.cancel();
            return;
        }

        Message message = BeaconEvents.getInstance().getSettings().getBroadcasts().getEvent();
        if (message.getFrequency() > 0) {
            if (System.currentTimeMillis() > nextEventMessage) {
                for (Player player : Bukkit.getOnlinePlayers()) {
                    if (manager.beaconInWorld(player.getWorld().getName())) {
                        for(String m : message.getMessages()) {
                            player.sendMessage(manager.formatString(m));
                        }
                    }
                }
                nextEventMessage = System.currentTimeMillis() + message.getFrequency() * 1000;
            }
        }
    }

}
