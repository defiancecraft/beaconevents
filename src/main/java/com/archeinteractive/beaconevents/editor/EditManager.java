package com.archeinteractive.beaconevents.editor;

import com.archeinteractive.beaconevents.config.components.Beacon;
import com.archeinteractive.beaconevents.config.components.Location;
import com.archeinteractive.beaconevents.editor.conversation.AddConvo;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import java.util.HashMap;
import java.util.Map;

public class EditManager implements Listener {

    private static EditManager instance;
    private Map<String, Editor> active = new HashMap<>();

    public boolean isActive(String player) {
        return active.containsKey(player.toLowerCase());
    }

    public void setInactive(String player) {
        if (isActive(player)) {
            active.remove(player.toLowerCase());
        }
    }

    public void setActive(String player) {
        if (isActive(player) == false) {
            active.put(player.toLowerCase(), new Editor());
        }
    }

    public Editor getEditor(String player) {
        if (isActive(player)) {
            return active.get(player.toLowerCase());
        }

        return null;
    }

    public EditManager() {
        instance = this;
    }

    public static EditManager getInstance() {
        return instance;
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onInteract(PlayerInteractEvent event) {
        if (isActive(event.getPlayer().getName())) {
            if (getEditor(event.getPlayer().getName()).getConversation() != null) {
                return;
            }

            if (event.getAction().equals(Action.LEFT_CLICK_BLOCK)) {
                Editor editor = getEditor(event.getPlayer().getName());
                editor.setConversation(new AddConvo(event.getPlayer().getName(), new Beacon(new Location(event.getClickedBlock()))));
                editor.setBlock(event.getClickedBlock());
                editor.start(event.getPlayer());
            }
        }
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onQuit(PlayerQuitEvent event) {
        if (isActive(event.getPlayer().getName())) {
            setInactive(event.getPlayer().getName());
        }
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onBlockBreak(BlockBreakEvent event) {
        for (Editor editor : active.values()) {
            if (editor.getBlock().equals(event.getBlock())) {
                event.setCancelled(true);
            }
        }
    }

}
