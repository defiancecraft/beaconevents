package com.archeinteractive.beaconevents.editor.conversation;

import com.archeinteractive.beaconevents.BeaconEvents;
import com.archeinteractive.beaconevents.config.components.Beacon;
import com.archeinteractive.beaconevents.config.components.Enchant;
import com.archeinteractive.beaconevents.config.components.Item;
import com.archeinteractive.beaconevents.editor.EditManager;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.conversations.*;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;

public class AddConvo implements ConversationAbandonedListener, IConversation {

    public String player;
    public Beacon beacon;
    public ConversationFactory factory;

    public AddConvo(String player, Beacon beacon) {
        this.player = player;
        this.beacon = beacon;
        this.factory = new ConversationFactory(BeaconEvents.getInstance())
                .withModality(true)
                .withPrefix(new NullConversationPrefix())
                .withFirstPrompt(new FrequencyPrompt())
                .withEscapeSequence("/quit")
                .withTimeout(90)
                .thatExcludesNonPlayersWithMessage("Go away evil console!")
                .addConversationAbandonedListener(this);
    }

    public void beginConversation(Player player) {
        player.beginConversation(factory.buildConversation(player));
    }

    @Override
    public void conversationAbandoned(ConversationAbandonedEvent conversationAbandonedEvent) {
        if (EditManager.getInstance().isActive(player)) {
            EditManager.getInstance().setInactive(player);
        }
    }

    private class FrequencyPrompt extends NumericPrompt {
        @Override
        public String getPromptText(ConversationContext conversationContext) {
            return ChatColor.DARK_GREEN + "Please input your desired frequency in seconds";
        }

        @Override
        protected Prompt acceptValidatedInput(ConversationContext conversationContext, Number number) {
            beacon.setFrequency(number.intValue());
            return new ItemListPrompt();
        }
    }

    private class ItemListPrompt extends StringPrompt {
        @Override
        public String getPromptText(ConversationContext conversationContext) {
            return ChatColor.DARK_GREEN + "Please specify an item name to add to the loot list or type " + ChatColor.GOLD + "DONE" + ChatColor.DARK_GREEN + " to finish adding items";
        }

        @Override
        public Prompt acceptInput(ConversationContext conversationContext, String input) {
            if (input.equalsIgnoreCase("DONE")) {
                finish();
                return END_OF_CONVERSATION;
            }

            String[] words = input.split(" ");
            if (words.length > 0) {
                Material material = Material.matchMaterial(words[0]);
                if (material != null) {
                    Item item = new Item(material.name());
                    beacon.getItems().add(item);
                    return new ItemQuantityPrompt(this, item);
                }
            }
            return this;
        }
    }

    private class ItemQuantityPrompt extends NumericPrompt {
        public ItemListPrompt prompt;
        public Item item;

        public ItemQuantityPrompt(ItemListPrompt prompt, Item item) {
            this.prompt = prompt;
            this.item = item;
        }

        @Override
        public String getPromptText(ConversationContext conversationContext) {
            return ChatColor.DARK_GREEN + "Enter an item stack size between 1 and 64";
        }

        @Override
        protected Prompt acceptValidatedInput(ConversationContext conversationContext, Number number) {
            item.setQuantity((number.intValue() > 0 && number.intValue() <= 64) ? number.intValue() : 1);
            return new ItemDataPrompt(prompt, item);
        }
    }

    private class ItemDataPrompt extends NumericPrompt {
        public ItemListPrompt prompt;
        public Item item;

        public ItemDataPrompt(ItemListPrompt prompt, Item item) {
            this.prompt = prompt;
            this.item = item;
        }

        @Override
        public String getPromptText(ConversationContext conversationContext) {
            return ChatColor.DARK_GREEN + "Enter the data value for the item";
        }

        @Override
        protected Prompt acceptValidatedInput(ConversationContext conversationContext, Number number) {
            item.setData(number.byteValue());
            return new ItemEnchantmentPrompt(prompt, item);
        }
    }

    private class ItemEnchantmentPrompt extends StringPrompt {
        public ItemListPrompt prompt;
        public Item item;

        public ItemEnchantmentPrompt(ItemListPrompt prompt, Item item) {
            this.prompt = prompt;
            this.item = item;
        }

        @Override
        public String getPromptText(ConversationContext conversationContext) {
            return ChatColor.DARK_GREEN + "Enter the name of the enchantment to add or type " + ChatColor.GOLD + "DONE" + ChatColor.DARK_GREEN + " to finish adding enchantments";
        }

        @Override
        public Prompt acceptInput(ConversationContext conversationContext, String input) {
            if (input.equalsIgnoreCase("DONE")) {
                return prompt;
            }

            String[] words = input.split(" ");
            if (words.length > 0) {
                Enchantment enchantment = Enchantment.getByName(words[0].toUpperCase());
                if (enchantment != null) {
                    return new ItemEnchantLevelPrompt(this, item, words[0].toUpperCase());
                }
            }

            return this;
        }
    }

    private class ItemEnchantLevelPrompt extends NumericPrompt {
        public ItemEnchantmentPrompt prompt;
        public Item item;
        public String enchantment;

        public ItemEnchantLevelPrompt(ItemEnchantmentPrompt prompt, Item item, String enchantment) {
            this.prompt = prompt;
            this.item = item;
            this.enchantment = enchantment;
        }

        @Override
        public String getPromptText(ConversationContext conversationContext) {
            return ChatColor.DARK_GREEN + "Enter the level for this enchantment";
        }

        @Override
        protected Prompt acceptValidatedInput(ConversationContext conversationContext, Number number) {
            item.getEnchantments().add(new Enchant(enchantment, number.intValue()));
            return prompt;
        }
    }

    public void finish() {
        BeaconEvents.getInstance().getSettings().getBeacons().add(beacon);
        BeaconEvents.getInstance().getSettings().setChanged(true);
    }

}
