package com.archeinteractive.beaconevents.editor.conversation;

import org.bukkit.entity.Player;

public interface IConversation {

    public void beginConversation(Player player);

}
