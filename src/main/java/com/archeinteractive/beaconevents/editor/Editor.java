package com.archeinteractive.beaconevents.editor;

import com.archeinteractive.beaconevents.editor.conversation.IConversation;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

// TODO: Prevent griefing of block being edited
public class Editor {

    private IConversation conversation = null;
    private Block block = null;

    public IConversation getConversation() {
        return conversation;
    }

    public void setConversation(IConversation conversation) {
        this.conversation = conversation;
    }

    public Block getBlock() {
        return block;
    }

    public void setBlock(Block block) {
        this.block = block;
    }

    public void start(Player player) {
        conversation.beginConversation(player);
    }
}
