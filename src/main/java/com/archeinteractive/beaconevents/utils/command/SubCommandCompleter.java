package com.archeinteractive.beaconevents.utils.command;

import java.lang.annotation.*;

/**
 * An annotation interface that may be attached to
 * a method to designate it as a subcommand tab completer.
 * When registering a handler with this class, only
 * methods marked with this annotation will be
 * considered for subcommand completion registration.
 */
@Inherited
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface SubCommandCompleter {

    String parent();
    
    String name();
}
