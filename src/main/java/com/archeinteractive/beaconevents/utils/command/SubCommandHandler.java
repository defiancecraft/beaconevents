package com.archeinteractive.beaconevents.utils.command;

import java.lang.annotation.*;

/**
 * An annotation interface that may be attached to
 * a method to designate it as a subcommand handler.
 * When registering a handler with this class, only
 * methods marked with this annotation will be
 * considered for subcommand registration.
 *
 * @originalauthor AmoebaMan
 */
@Inherited
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface SubCommandHandler {
    String parent();

    String name();

    String usage() default "";

    String permission() default "";

    String permissionMessage() default "You do not have permission to use that command";
}
