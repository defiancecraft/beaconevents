package com.archeinteractive.beaconevents;

import com.archeinteractive.beaconevents.config.components.Beacon;
import com.archeinteractive.beaconevents.config.components.Broadcasts;
import com.archeinteractive.beaconevents.config.components.Message;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

import java.util.*;
import java.util.concurrent.TimeUnit;

public class BeaconManager implements Listener {

    private BukkitTask task;
    private long next = calculateNextEvent();
    private List<Long> pre = calculatePreMessages();
    private boolean inProgress = false;

    public BeaconManager() {
        Bukkit.getPluginManager().registerEvents(this, BeaconEvents.getInstance());
        task = Bukkit.getScheduler().runTaskTimer(BeaconEvents.getInstance(),
                () -> startEvent(),
                20,
                20);
    }

    public ArrayList<Beacon> getBeacons() {
        return BeaconEvents.getInstance().getSettings().getBeacons();
    }

    public boolean isBeaconBlock(Block block) {
        Location location = block.getLocation();
        for (Beacon beacon : getBeacons()) {
            if (beacon.getLocation().getX() == location.getX()
                    && beacon.getLocation().getY() == location.getY()
                    && beacon.getLocation().getZ() == location.getZ()) {
                return true;
            }
        }
        return false;
    }

    public boolean beaconBlockExist(Beacon beacon) {
        Block block = Bukkit.getWorld(beacon.getLocation().getWorld()).getBlockAt(beacon.getLocation().getX(), beacon.getLocation().getY(), beacon.getLocation().getZ());
        if (block.getType() != Material.AIR) {
            return true;
        }
        return false;
    }

    public boolean beaconInWorld(String world) {
        for (Beacon beacon : getBeacons()) {
            if (beacon.getLocation().getWorld().equalsIgnoreCase(world)) {
                return true;
            }
        }

        return false;
    }

    public void startEvent() {
        if (System.currentTimeMillis() > next) {
            for (Beacon beacon : BeaconEvents.getInstance().getSettings().getBeacons()) {
                World world = Bukkit.getWorld(beacon.getLocation().getWorld());
                for (Player player : world.getPlayers()) {
                    for (String message : BeaconEvents.getInstance().getSettings().getBroadcasts().getEventStart()) {
                        player.sendMessage(formatString(message));
                    }
                }
                if (beaconBlockExist(beacon)) {
                    new BeaconEventTask(beacon).runTaskTimer(BeaconEvents.getInstance(), 0, beacon.getFrequency() * 20);
                }
            }
            new BeaconAnnounceTask(this, next + BeaconEvents.getInstance().getSettings().getEventDuration() * 1000).runTaskTimer(BeaconEvents.getInstance(), 0, 20);
            next = calculateNextEvent();
            pre = calculatePreMessages();
            inProgress = true;
            Bukkit.getScheduler().runTaskLater(BeaconEvents.getInstance(),
                    () -> inProgress = false,
                    BeaconEvents.getInstance().getSettings().getEventDuration() * 20);
        } else {
            if (pre.size() > 0) {
                if (System.currentTimeMillis() > pre.get(pre.size() - 1).longValue()) {
                    Message message = BeaconEvents.getInstance().getSettings().getBroadcasts().getPreEvent();
                    for (Beacon beacon : getBeacons()) {
                        World world = Bukkit.getWorld(beacon.getLocation().getWorld());
                        for (Player player : world.getPlayers()) {
                            for (String m : message.getMessages()) {
                                player.sendMessage(formatString(m));
                            }
                        }
                    }
                    pre.remove(pre.size() - 1);
                }
            }
        }
    }

    public long calculateNextEvent() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        long next = System.currentTimeMillis();
        boolean found = false;
        while (found == false) {
            if (calendar.getTimeInMillis() < next) {
                calendar.add(Calendar.MINUTE, BeaconEvents.getInstance().getSettings().getEventInterval());
            } else {
                found = true;
                next = calendar.getTimeInMillis();
            }
        }

        return next;
    }

    public List<Long> calculatePreMessages() {
        List<Long> list = new LinkedList<>();
        Message message = BeaconEvents.getInstance().getSettings().getBroadcasts().getPreEvent();
        if (message.getQuantity() > 0 && message.getFrequency() > 0) {
            for (int i = 1; i <= message.getQuantity(); i++) {
                list.add(next - (i * message.getFrequency() * 1000) - 1000);
            }
        }
        return list;
    }

    public String formatTime(long millis) {
        long minutes = TimeUnit.MILLISECONDS.toMinutes(millis);
        if (minutes > 0) {
            return String.format("%d minutes, %d seconds",
                    TimeUnit.MILLISECONDS.toMinutes(millis),
                    TimeUnit.MILLISECONDS.toSeconds(millis) -
                            TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
        } else {
            return String.format("%d seconds",
                    TimeUnit.MILLISECONDS.toSeconds(millis) -
                            TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis))
            );
        }
    }

    public String formatString(String message) {
        message = ChatColor.translateAlternateColorCodes('&', message);

        if (message.contains("%t")) {
            message = message.replace("%t", formatTime(next - System.currentTimeMillis()));
        }

        return message;
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onBlockBreak(BlockBreakEvent event) {
        if (isBeaconBlock(event.getBlock())) {
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onPlayerInteract(PlayerInteractEvent event) {
        if (event.getClickedBlock() != null) {
            if (isBeaconBlock(event.getClickedBlock())) {
                if (event.getAction() == Action.RIGHT_CLICK_BLOCK) {
                    Material material = event.getClickedBlock().getType();
                    if (material == Material.BEACON || material == Material.CHEST || material == Material.FURNACE
                            || material == Material.BURNING_FURNACE || material == Material.ENCHANTMENT_TABLE
                            || material == Material.ENCHANTED_BOOK || material == Material.WORKBENCH
                            || material == Material.HOPPER || material == Material.ANVIL
                            || material == Material.BREWING_STAND || material == Material.CAKE
                            || material == Material.DISPENSER || material == Material.DROPPER
                            || material == Material.ENDER_CHEST || material == Material.COMMAND) {
                        event.setCancelled(true);
                    }
                }

                Broadcasts broadcasts = BeaconEvents.getInstance().getSettings().getBroadcasts();
                if (inProgress) {
                    for (String message : broadcasts.getEvent().getMessages()) {
                        String m = message;
                        m = formatString(message);
                        event.getPlayer().sendMessage(message);
                    }
                } else {
                    String message = broadcasts.getBeaconClick();
                    message = formatString(message);
                    event.getPlayer().sendMessage(message);
                }
            }
        }
    }

}
