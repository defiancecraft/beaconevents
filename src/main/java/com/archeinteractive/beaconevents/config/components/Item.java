package com.archeinteractive.beaconevents.config.components;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;

public class Item {
    private String name = "STONE";
    private byte data = 0;
    private int quantity = 1;
    private List<Enchant> enchantments = new ArrayList<Enchant>();

    public Item() {}

    public Item(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getData() {
        return data;
    }

    public void setData(byte data) {
        this.data = data;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public List<Enchant> getEnchantments() {
        return enchantments;
    }

    public void setEnchantments(List<Enchant> enchantments) {
        this.enchantments = enchantments;
    }

    public ItemStack createItemStack() {
        Material material = Material.matchMaterial(name.toUpperCase());
        if (material == null) {
            return null;
        }
        ItemStack itemStack = new ItemStack(material, quantity, data);
        if (enchantments.size() > 0) {
            for (Enchant enchant : enchantments) {
                Enchantment enchantment = Enchantment.getByName(enchant.getName().toUpperCase());
                if (enchant != null) {
                    itemStack.addUnsafeEnchantment(enchantment, enchant.getLevel());
                }
            }
        }
        return itemStack;
    }
}
