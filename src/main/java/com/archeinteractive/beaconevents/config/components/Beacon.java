package com.archeinteractive.beaconevents.config.components;

import java.util.ArrayList;

public class Beacon {

    private Location location = new Location();
    private int frequency = 2;
    private ArrayList<Item> items = new ArrayList<Item>();

    public Beacon() {}

    public Beacon(Location location) {
        this.location = location;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public int getFrequency() {
        return frequency;
    }

    public void setFrequency(int frequency) {
        this.frequency = frequency;
    }

    public ArrayList<Item> getItems() {
        return items;
    }

    public void setItems(ArrayList<Item> items) {
        this.items = items;
    }
}
