package com.archeinteractive.beaconevents.config.components;

import java.util.ArrayList;

public class Broadcasts {

    private Message preEvent = new Message(new ArrayList<String>(){{
        add("Item drop event will begin in %t!");
    }});
    private ArrayList<String> eventStart = new ArrayList<String>(){{
        add("Item drop event has started!");
    }};
    private Message event = new Message(new ArrayList<String>(){{
        add("Item drop event is in progress!");
    }});
    private ArrayList<String> postEvent = new ArrayList<String>(){{
        add("The item drop event has ended!");
    }};
    private String beaconClick = "Item drop event will begin in %t!";

    public Message getPreEvent() {
        return preEvent;
    }

    public ArrayList<String> getEventStart() {
        return eventStart;
    }

    public Message getEvent() {
        return event;
    }

    public ArrayList<String> getPostEvent() {
        return postEvent;
    }

    public String getBeaconClick() {
        return beaconClick;
    }
}
