package com.archeinteractive.beaconevents.config.components;

import java.util.ArrayList;

public class Message {

    private ArrayList<String> messages;
    private int frequency = 5;
    private int quantity = -1;

    public Message(ArrayList<String> messages) {
        this.messages = messages;
    }

    public ArrayList<String> getMessages() {
        return messages;
    }

    public int getFrequency() {
        return frequency;
    }

    public int getQuantity() {
        return quantity;
    }
}
