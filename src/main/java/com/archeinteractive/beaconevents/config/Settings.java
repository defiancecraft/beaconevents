package com.archeinteractive.beaconevents.config;

import com.archeinteractive.beaconevents.config.components.Beacon;
import com.archeinteractive.beaconevents.config.components.Broadcasts;
import com.archeinteractive.beaconevents.utils.JsonConfig;

import java.util.ArrayList;

public class Settings extends JsonConfig {

    private int eventInterval = 60;
    private int eventDuration = 120;
    private Broadcasts broadcasts = new Broadcasts();
    private ArrayList<Beacon> beacons = new ArrayList<Beacon>();
    private transient boolean changed = false;

    public int getEventInterval() {
        return eventInterval;
    }

    public void setEventInterval(int eventInterval) {
        this.eventInterval = eventInterval;
    }

    public int getEventDuration() {
        return eventDuration;
    }

    public void setEventDuration(int eventDuration) {
        this.eventDuration = eventDuration;
    }

    public Broadcasts getBroadcasts() {
        return broadcasts;
    }

    public void setBroadcasts(Broadcasts broadcasts) {
        this.broadcasts = broadcasts;
    }

    public ArrayList<Beacon> getBeacons() {
        return beacons;
    }

    public void setBeacons(ArrayList<Beacon> beacons) {
        this.beacons = beacons;
    }

    public boolean isChanged() {
        return changed;
    }

    public void setChanged(boolean changed) {
        this.changed = changed;
    }
}
